.model small
.model tiny
org 100h
.stack 100h 

.data 

time db 1      ;time per frame=time*.05sec->delay speed
column db 80 dup (0)     ;duplicate the data on screen resolution
column2 db 80 dup (65) 
seed dw ?
temp dw 0     
timet db 0     
timelevel db 0     
score1 db 30h         ;3rd digit of score default is 0
score2 db 30h         ;2nd digit of score is 0
life db 50
ctemp db 0     ;char temp  
stemp dw 0     ;start position temp  
ktemp dw 0     ;keyboard input temp
uiC db 0       ;counter for ui column

;Message
msg db "TYPING BOSS! "
msg1 db "choose level to start :  "
msg2 db "1.Easy"
msg3 db "2.Medium"
msg4 db "3.Hard" 
msg5 db "Enjoy The Game. . . ."
msg6 db "Score: "
msg7 db "Life: "
msg8 db "GameOver!"
msg9 db "Your Score: "
msg0 db ". . .Press any key to exit" 

.code      

main:
call randinit
mov ah,00h           ;graphics mode set
mov al,03h         ;set size 80*25
int 10h
mov ch,32          ;hide cursor
mov ah,1
int 10h    

ui:
mov bh,00h
mov ah,2
mov dl,uiC
mov dh,19
int 10h
mov ah,9
mov bl,12         ;color
mov bh,00
mov cx,1         ;number of char
mov al,178
int 10h
add uiC,1
cmp uiC,80
je menu
jb ui


;////////////print start menu/////////////

menu:
lea bp,msg        ;print msg string
mov ah,13h
mov bh,0h
mov bl,9          ;color
mov al,00h        ;write mode
mov cx,12         ;number of char in str
mov dl,35         ;column
mov dh,3          ;row
int 10h  

lea bp,msg1       ;print msg1 string
mov ah,13h
mov bh,0h
mov bl,14          ;color
mov al,00h        ;write mode
mov cx,24         ;number of char in str
mov dl,29         ;column
mov dh,5          ;row
int 10h              

lea bp,msg2       ;print msg2 string
mov ah,13h
mov bh,0h
mov bl,3          ;color
mov al,00h        ;write mode
mov cx,6         ;number of char in str
mov dl,20         ;column
mov dh,7          ;row
int 10h     

lea bp,msg3       ;print msg3 string
mov ah,13h
mov bh,0h
mov bl,10          ;color
mov al,00h        ;write mode
mov cx,8         ;number of char in str
mov dl,20         ;column
mov dh,8          ;row
int 10h  

lea bp,msg4       ;print msg4 string
mov ah,13h
mov bh,0h
mov bl,11          ;color
mov al,00h        ;write mode
mov cx,6         ;number of char in str
mov dl,20         ;column
mov dh,9         ;row
int 10h    

lea bp,msg5       ;print msg5 string
mov ah,13h
mov bh,0h
mov bl,13          ;color
mov al,00h        ;write mode
mov cx,21         ;number of char in str
mov dl,20         ;column
mov dh,12          ;row
int 10h  

;/////////chack selected from keyboard//////////
               
select:
mov ah,0          ;get char to al
int 16h           ;choose difficulty
cmp al,49         ;check 1 for easy
jne select1
mov time,15
jmp start

select1:
cmp al,50         ;check 2 for medium
jne select2
mov time,10
jmp start

select2:
cmp al,51         ;check 3 for hard
jne select3
mov time,5
jmp start

select3:
cmp al,27         ;check esc
jne select
jmp terminate           

;//////////random function//////////
randinit:              ;change seed each frame
mov ah,0               ;get system time
int 1ah                ;cx:dx now hold number of clock ticks since midnight
push dx
pop seed               ;move seed to dx
ret

randcol:               ;rand algorithm by using seed from randinit
mov ax,seed
add seed,23
xor dx,dx
mov cx,60
div cx                 ;dx contains the remainder of the division from 0 to 59
mov bx,dx
add bx,15              ;shift screen position
ret                    ;result random number to al

randtype:              ;rand type to display
mov ax,seed
xor dx,dx
mov cx,3
div cx
ret

randchar:              ;select type from randtype
call randtype
cmp dl,0
je randnum
cmp dl,1
je randupper
cmp dl,2
je randlower

randnum:               ;rand number
mov ax,seed
add seed,1
xor dx,dx
mov cx,10
div cx
add dl,48              ;dl contain rand num 0-9
mov al,dl
ret

randupper:             ;rand capital characters
mov ax,seed
add seed,1
xor dx,dx
mov cx,26
div cx                 ;dx contain the remainder of the division from 0-93
add dl,65              ;dl contain rand char A-Z
mov al,dl
ret

randlower:             ;rand lower characters
mov ax,seed
add seed,1
xor dx,dx
mov cx,26
div cx                 ;dx contain the remainder of the division from 0-93
add dl,97              ;dl contain rand char a-z
mov al,dl
ret

;//////////screen settings///////////

clear:             ;clear screen
mov ah,2 
mov bh,0
mov dh,ctemp
mov dl,0
int 10h
mov ah,9
mov bh,0
mov al," "
mov bl,00
mov cx,80
int 10h
inc ctemp
cmp ctemp,80
jne clear
mov ctemp,0  
ret

setpos:            ;position set
mov bx,temp
mov dx,temp
mov dh,column[bx]
dec column[bx]
mov bh,0
mov ah,02h
int 10h            ;set cursor
ret 
           
;////////////Timer////////////

timer:                 ;get time from system
mov ah,2ch 
int 21h                ;delay a time estimate 5*time mili sec
mov timet,dl           ;set the game speed by level
mov ah,time
mov timelevel,ah
call printscore
call printlife   

timer2:                  ;modified time speed for level
call checkkey
mov ah,2ch
int 21h
cmp dl,timet
je timer2
dec timelevel
mov timet,dl
cmp timelevel,0
jne timer2
ret      


;///////////print function//////////  
printc:              ;print char one by one  
mov ah,09h
mov bh,00h
mov cx,1
int 10h
ret
 
printword:
call setpos
mov bx,temp
mov al,column2[bx]
mov bl,0ah              ;white color
call printc
call setpos
mov al,' '              ;del last char
mov bl,00h
call printc
mov bx,temp
add column[bx],3
cmp column[bx],26
jne printword2
call declife
mov column[bx],0  

printword2:
ret

printscore:
mov bh,00
mov ah,2
mov dl,11
mov dh,18
int 10h
mov ah,9
mov bl,6
mov bh,00
mov cx,1
mov al,score1
int 10h
mov bh,00
mov ah,2
mov dl,10
mov dh,18
int 10h
mov ah,9
mov bl,6
mov bh,00
mov cx,1
mov al,score2
int 10h
;check 0-9
cmp score1,39h
jne oout
je plus10 

oout:
ret

plus10:
mov score1,30h
add score2,1
ret 

printlife:
mov bh,00
mov ah,2
mov dl,10
mov dh,19
int 10h
mov ah,9
mov bl,2
mov bh,00
mov cx,1
mov al,life
int 10h
ret    
                     
declife:
dec life
cmp life,48         ;ascii code of 0
jne declife2
jmp gameover

declife2:
ret                     

;/////////////////

startcol:  
call randcol
cmp column[bx],0       ;row
jne startcol
inc column[bx]
mov stemp,bx 
call randchar
mov bx,stemp
mov column2[bx],dl 
cc:ret

aa:
inc bx

checkandprint:           ;check each line to move forward
cmp bx,80
je bb  
cmp column[bx],0
je aa
mov temp,bx
call printword
mov bx,temp
jmp aa


bb:
ret

;//////////////input function///////////

checkkey:
mov ah,1
int 16h              ;using int 16h to get input from keyboard
jnz keycheck
ret

keycheck:          ;check when've any input
mov ah,0
int 16h
cmp al,27
jne check
jmp terminate

check:
mov ktemp,80

check1:
dec ktemp
cmp ktemp,-1
je outa
mov bx,ktemp
cmp column2[bx],al
jne check1
dec column[bx]
mov ah,02
mov dx,ktemp
mov dh,column[bx]
mov bh,00
int 10h
mov al," "
mov bh,0
mov cx,1
mov bl,00h
mov ah,9
int 10h
mov bx,ktemp
mov column[bx],0
mov column2[bx],0
inc score1   

outa:
ret        
           
;////////////Game start///////////

start: 
call clear
lea bp,msg6        ;print your score
mov ah,13h
mov bh,0h
mov bl,10
mov al,00h
mov cx,7
mov dl,2
mov dh,18
int 10h

lea bp,msg7        ;print life
mov ah,13h
mov bh,0h
mov bl,11          ;color
mov al,00h         ;write mode
mov cx,6           ;numb of char
mov dl,2           ;column
mov dh,19          ;row
int 10h       

gamestart:          ;loop the game
call startcol
xor bx,bx    
call checkandprint
call timer
jmp gamestart  

;////////////game over///////////

gameover:
call clear
lea bp,msg8              ;game over
mov ah,13h
mov bh,0h
mov bl,4
mov al,00h
mov cx,10
mov dl,35
mov dh,6
int 10h

lea bp,msg9              ;your score
mov ah,13h
mov bh,0h
mov bl,15
mov al,00h
mov cx,11
mov dl,22
mov dh,9
int 10h 

;print score
mov bh,00
mov ah,2
mov dl,35
mov dh,9
int 10h
mov ah,9
mov bl,15
mov bh,00
mov cx,1
mov al,score2
int 10h
mov bh,00
mov ah,2
mov dl,36
mov dh,9
int 10h
mov ah,9
mov bl,15
mov bh,00
mov cx,1
mov al,score1
int 10h

lea bp,msg0
mov ah,13h
mov bh,0h
mov bl,6
mov al,0
mov cx,26
mov dl,20
mov dh,12
int 10h

mov ah,00h
int 16h
jmp terminate

;////////exit function///////////

terminate:
call clear
mov ah, 4ch
mov al,00
int 21h
                           

ret          

end main 
